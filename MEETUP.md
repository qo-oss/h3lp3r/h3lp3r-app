## Installation

Install last node LTS version with npm

    npm i -g expo-cli @expo/ngrok
    
Create initial proyect (choose Tabs with typescript)

    expo init qo-rn-meetup 
    # We choose "tabs (Typescript)"
    code qo-rn-meetup

    # From VSCode console
    expo start

Ensure the device is connected to adb (Android Debug Bridge) and unlock it

    adb devices
    
From expo console install Expo Go and launch the app (type: 'a') or from expo web console you can click on [Run on Android device/emulator]

## Modifing default app

Let's play a bit with app literals, colors and icons, changing the existing application

Open `BottomTabNavigator.tsx`

Modify tab icons (ie: home, planet, settings). Notice the name typescript validation from VSCode with `name: React.ComponentProps<typeof Ionicons>['name']`

Add backgroundColors to `tabBarOptons`

    activeBackgroundColor: '#006', inactiveBackgroundColor: '#ccccdd' 

Add backGround color to header in `TabOneNavigator()` and `TabTwoNavigator()`, in `options` property:

    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#006',          
    } 

With header changed the StatusBar should be changed, using expo with should use the Expo doc: https://docs.expo.io/versions/latest/sdk/status-bar/

Searching StatusBar we'll find it in `App.txs` file, we'll add the `"light"` style:

    <StatusBar animated={true} style="light" />

On file `TabOneScreen.tsx` we can modify:

Title: `<Text style={styles.title}>Welcome to my Meetup</Text>` 

Title style:

    title: {
      backgroundColor: '#eee',
      borderWidth: 1,
      padding: 10,
      borderRadius: 10,

Layout:

    container: {
      flex: 1,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'flex-start',
    },

Adding an Icon under the title:

    <Ionicons size={80} style={{ marginTop: 20 }} name="airplane" color="orange" />

On component file `EditScreenInfo.tsx`:

Layout on main `View` to center content:

    <View style={{ flex: 1, justifyContent: 'center'}}>


## Preparing cloud building

Stop expo session

Changing the app icon on `app.json`:

    "icon": "./src/assets/images/appicon.png",

Remove or replace the adaptative icon in Android

Creating a new build in the cloud

    expo build:android

Accesing to https://expo.io/ to check the build status


## Cloning existing app H3lp3r

From GitLab repo and `meetup` branch:

    git clone https://gitlab.com/qo-oss/h3lp3r/h3lp3r-app.git
    git checkout -b meetup

Before of all we download project deps:

    yarn install # or npm install

We create a new expo session for the new app:

    expo start --android
    # Maybe you need to relaod the app (`r`)

## Adding Favs to H3lp3r app

In `meetup_source_code.md` file will find all the necessary code to implement the Favs feature in our app but we'll try to do it step by step.

First of all we need a state management system, for our app we've choose MST ([Mobx-State-Tree](https://mobx-state-tree.js.org/))

We are going to create a new "Store" to save the "favs" actions, so So we can open the file `src/store/index.ts` where we'll find the `Favorites` model, in the model there are modeled data and the actions to operate on the data, every time an action si called a new "snapshot" is created.

With the model created, we need an icon on every action to store it as a favorite action, the best place is the navigator header en `SideMenuNavigator`. The icon should be different if the action is already in favourites list or not. When the icon is pressed the action should added or removed to favourites list based on if the action is already included or not in the list.

Now, with a model that is updated with favourite actions, we need to show them all in the `DashboardScreeen.tsx` where we need to access to model data and using a callback be notified when the favourites list is changed.




