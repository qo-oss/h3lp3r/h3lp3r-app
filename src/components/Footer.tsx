
import React from "react";
import { Image, Linking, Platform, Pressable, StyleSheet, Text, View } from "react-native";
import H3lp3rColors from "@app/constants/Colors";
import { MONOSPACE_FONT_FAMLIY, URL_WEB_H3LP3R } from "@app/constants";
import { version } from '../../package.json';


export function Footer() {

  function goToH3lp3rWeb() {
    Linking.openURL(URL_WEB_H3LP3R);
  }
  return (    
    <Pressable style={styles.container} onPress={goToH3lp3rWeb} >
      <Image resizeMode="contain" source={require("@app/assets/images/logo-banner-h3lp3r.png")} style={styles.logo} ></Image>
      <Text style={styles.copyright}>v.{version} - Quality Objects ©2021</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 5,
    flexDirection: 'column'
  },
  logo: {    
    marginTop: 10,
    width: 250,
    height: 35
  },
  copyright: {
    textAlign: 'center',
    fontSize: 10,
    color: H3lp3rColors.TEXT_HINT_COLOR
  },
  result_text: {
    flex: 1,
    marginRight: 5,
    padding: 5,
    fontFamily: MONOSPACE_FONT_FAMLIY,
    fontSize: 18,
  }
});

