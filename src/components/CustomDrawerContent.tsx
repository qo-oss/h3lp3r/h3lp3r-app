import { URL_WEB_QO } from "@app/constants";
import H3lp3rColors from "@app/constants/Colors";
import { ipService } from "@app/services";
import { Ionicons } from "@expo/vector-icons";
import { DrawerItemList } from "@react-navigation/drawer";
import React, { useEffect, useState } from "react";
import { Image, Linking, Pressable, ScrollView, StyleSheet, Text, View } from "react-native";



export function CustomDrawerContent(props?: any) {
    const [myIp, setMyIp] = useState<string>();
    
    useEffect(() => {
        async function fetchIp() {
            setMyIp(await ipService.getIP())
        }
        fetchIp();
    }, []);

    function goToQOWeb() {
        Linking.openURL(URL_WEB_QO);
    }


    const itemProps = {
        activeTintColor: H3lp3rColors.MAIN_COLOR,
        activeBackgroundColor: '#f0f0f0',
        inactiveTintColor: H3lp3rColors.TEXT_COLOR
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.header} >
                <Ionicons size={32} name="close" color={H3lp3rColors.TEXT_COLOR} onPress={() => props.navigation.closeDrawer()} />
            </View>
            <ScrollView style={{flex: 1}} >
                <DrawerItemList {...props} {...itemProps} />
            </ScrollView>
            <Pressable style={styles.logo_container} onPress={goToQOWeb} >
                <Text style={styles.footer_text}>Developed by:</Text>
                <Image style={styles.logo} resizeMode="contain" source={require("@app/assets/images/qo_logo.png")} ></Image>
                <Text style={styles.footer_text}>Yout IP: {myIp}</Text>
            </Pressable>
        </View>
    );
}


const styles = StyleSheet.create({
    header: {
        color: H3lp3rColors.TEXT_COLOR,
        width: '100%',
        paddingTop: 32,
        paddingHorizontal: 10,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    close_icon: {

        width: 20,
        height: 20,
    },
    logo_container: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 5,
        flexDirection: 'column'
    },
    logo: {
        marginVertical: 5,
        width: 250,
        height: 35
    },
    footer_text: {
        textAlign: 'center',
        fontSize: 10,
        color: H3lp3rColors.TEXT_HINT_COLOR
    },
});

