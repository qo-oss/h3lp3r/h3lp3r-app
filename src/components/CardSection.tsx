
import H3lp3rColors from "@app/constants/Colors";
import { H3lp3rIcon } from "@app/types";
import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, View } from "react-native";


export function CardSection(props: {title: string, icon: H3lp3rIcon, children?: any}) {
  const { title, icon, children } = props;
  return <View style={cardStyles.cardContainer}>
    <View style={cardStyles.titleContainer}>
      <Ionicons style={cardStyles.icon} size={26} name={icon}  />
      <Text style={cardStyles.title} >{title}</Text>
    </View>
    {children}
  </View>;
}

const cardStyles = StyleSheet.create({
  cardContainer: {
    marginHorizontal: 10,
    marginVertical: 10,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 25,
    borderRadius: 10,
    opacity: 1,
    elevation: 1,
    borderColor: H3lp3rColors.MAIN_COLOR,
    borderWidth: 1,
    backgroundColor: H3lp3rColors.BACKGROUND_COLOR_SECTION,
    flexDirection: 'column',
    alignItems: 'stretch',    
    alignSelf: 'stretch',

    flex: 0,
    flexGrow: 0,
    flexShrink: 1,
  },
  titleContainer: { 
    marginHorizontal: 10,    
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',    
  },
  icon: {
    color: H3lp3rColors.TEXT_COLOR_TITLE,
  },
  title: {
    color: H3lp3rColors.TEXT_COLOR_TITLE,
    fontSize: 20,
    marginStart: 10,
    fontWeight: 'bold',
  },
});
