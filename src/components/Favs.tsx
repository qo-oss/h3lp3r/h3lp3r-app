
import H3lp3rColors from "@app/constants/Colors";
import { H3lp3rIcon } from "@app/types";
import { Ionicons } from "@expo/vector-icons";
import { NavigationProp } from "@react-navigation/core";
import React from "react";
import { FlatList, ListRenderItemInfo, Pressable, StyleSheet, Text, View } from "react-native";
import { CardSection } from "./CardSection";


export function FavItem(props: {title: string, routeName: string, icon: H3lp3rIcon, navigation: NavigationProp<any>}) {
  const { title, icon, routeName, navigation} = props;

  function goTo() {
    navigation.navigate(routeName);
  }

  return <View style={FavItem.styles.container}>
    <Pressable style={({ pressed }) => [{
        elevation: pressed ? 1 : 4,
        ...FavItem.styles.button        
      }]}
       onPress={goTo}>
      <Ionicons style={FavItem.styles.icon} size={24} name={icon}  />
      <Text style={FavItem.styles.title} >{title}</Text>
      <Ionicons style={[FavItem.styles.icon, {color: H3lp3rColors.MAIN_COLOR}]} size={24} name="navigate-circle-outline"  />
    </Pressable>    
  </View>;
}

FavItem.styles = StyleSheet.create({
  container: {
    marginVertical: 8,
    marginHorizontal: 15,
    alignSelf: 'stretch',    
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  title: {
    flex: 1,
    color: H3lp3rColors.TEXT_COLOR,
    fontSize: 16,
    marginStart: 10
  },
  icon: {
    color: H3lp3rColors.TEXT_COLOR
  },
  button: {
    flex: 1, 
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 10,
    height: 40,
    minWidth: 140,
    opacity:  1,
    backgroundColor: H3lp3rColors.BACKGROUND_COLOR,
    flexDirection: 'row',
    justifyContent: 'flex-start', 
    alignItems: 'center',
  },
});



export function FavsSection(props: {favs: any[], navigation: NavigationProp<any>}) {
  const { favs, navigation } = props;
  function renderFavItem(element: ListRenderItemInfo<any>) {
    const fav = element.item;
    return (<FavItem key={fav.name} 
                      title={fav.title} 
                      routeName={fav.name} 
                      icon={fav.icon} 
                      navigation={navigation} />);
  }
  return (<CardSection title="Favs" icon="star-outline" >
    <FlatList style={{ flex: 0, flexGrow: 1 }}
          data={favs}
          keyExtractor={(item) => item.name}
          renderItem={(item) => renderFavItem(item)}
        />
    {!favs?.length && <Text style={{color: H3lp3rColors.TEXT_HINT_COLOR, textAlign: 'center'}}>No favs</Text>}
          </CardSection>);
}

