import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';
import { Pressable, PressableProps, StyleProp, StyleSheet, Text as DefaultText, Text, View as DefaultView, ViewStyle } from 'react-native';

import Colors from '../constants/Colors';


export type ActionButtonProps = PressableProps & {
  title: string,   
  action: (event: any) => void, 
  icon?: keyof typeof Ionicons.glyphMap,
  tintColor?: string
}

export function ActionButton(props: ActionButtonProps) {
  const {title, icon, action, tintColor} = props;
  const containerStyle = (props.style || {}) as StyleProp<ViewStyle>;
  return (
    <Pressable 
      onPress={action} 
      style={({ pressed }) => [{
        elevation: pressed ? 1 : 8,
        ...actionButtonStyles.pressable        
      }, containerStyle]} >
      {!!icon && <Ionicons name={icon} size={28} color={tintColor} /> }
      <Text style={{        
        color: tintColor || Colors.TEXT_COLOR,
        ...actionButtonStyles.text
      }}>{title}</Text>      
    </Pressable>
  );
}

const actionButtonStyles = StyleSheet.create({
  pressable: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 20,
    height: 48,
    minWidth: 140,
    opacity:  1,
    backgroundColor: Colors.MAIN_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between', 
    alignItems: 'center',
  },
  text: {
    marginLeft: 5,
    fontWeight: "400",
    fontSize: 14

  }

})

type IconButtonProps = PressableProps & {
  icon: keyof typeof Ionicons.glyphMap,
  action: (event: any) => void, 
  size?: number, 
  tintColor?: string, 
  bgColor?: string,  
}

export function IconButton(props: IconButtonProps) {
  const {icon, action, size, tintColor, bgColor, ...others} = props;
  const DEFAULT_SIZE = 28;
  return (
    <Pressable onPress={action} {...others}
    style={({ pressed }) => ({            
      height: (size || DEFAULT_SIZE) + 10,
      width: (size || DEFAULT_SIZE) + 10,
      backgroundColor: bgColor || Colors.BACKGROUND_COLOR,      
      elevation: pressed ? 1 : 8,
      ...iconButtonStyles.pressable
    })} >
      <Ionicons name={icon} size={size || DEFAULT_SIZE} color={tintColor} /> 
    </Pressable>
  );
}

const iconButtonStyles = StyleSheet.create({
  pressable: {
    padding: 5,
    borderRadius: 5,
    opacity:  1,
    alignItems: 'center',
  }
});