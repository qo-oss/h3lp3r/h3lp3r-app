
import H3lp3rColors from "@app/constants/Colors";
import React from "react";
import { Keyboard, Pressable, StyleSheet, Text, View } from "react-native";
import { StringResult } from "./StringResult";
import { TextAreaWithIcon } from "./TextAreaWithIcon";
import { ActionButton } from "./Themed";


export type FormConfigData = {
  inputText?: string;
  title: string;
  actionLabel?: string;
  actionCaller: (text: string) => Promise<string>;
}

// https://dev.to/theodesp/best-ways-to-use-react-hooks-for-state-management-44h3

export function StringForm(props: FormConfigData) {
  const { inputText, title, actionCaller, actionLabel } = props;
  const [text, setText] = React.useState(inputText);
  const [result, setResult] = React.useState('');

  async function executeAction() {
    setResult(await actionCaller(text || ''));
  }

  return (
    <Pressable onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
    <View style={styles.container}>
      <Text style={styles.input_label}>Input text to apply action ({title})</Text>
      <TextAreaWithIcon text={text} onChangeText={(txt) => setText(txt)} ></TextAreaWithIcon>
      {!!result && <StringResult result={result}  />}
      <ActionButton icon="play" title={actionLabel || 'Execute'} 
        action={executeAction} 
        tintColor={H3lp3rColors.MAIN_COLOR_REVERSE} 
        style={{ marginTop: 20 }} />
        </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  input_label: {
    textAlign: 'left',
    fontSize: 12,
    color: H3lp3rColors.TEXT_HINT_COLOR,
    width: '80%',
  },
});

