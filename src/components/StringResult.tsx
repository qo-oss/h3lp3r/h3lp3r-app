import { MONOSPACE_FONT_FAMLIY } from '@app/constants';
import H3lp3rColors from '@app/constants/Colors';
import Clipboard from 'expo-clipboard';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { IconButton } from './Themed';


export function StringResult(props: { result: string, hideCopy?: boolean }) {
    const { result, hideCopy } = props;
    const [copying, setCopying] = React.useState(false);
  
    function copyText() {
      Clipboard.setString(result);
    }
  
    return (
        <View style={styles.result_container}>
            <Text style={{
                color: copying ? H3lp3rColors.MAIN_COLOR : H3lp3rColors.TEXT_RESULT_COLOR,
                ...styles.result_text
            }} >{result}</Text>
            {!hideCopy && <IconButton onPressIn={() => setCopying(true)}
                onPressOut={() => setCopying(false)}
                icon="copy-outline" size={24}
                bgColor={H3lp3rColors.BACKGROUND_COLOR}
                tintColor={H3lp3rColors.TEXT_RESULT_COLOR} action={copyText} />}
        </View>
    );
}

const styles = StyleSheet.create({
    result_container: {
      width: '80%',
      marginTop: 10,
      borderWidth: 1,
      borderColor: H3lp3rColors.MAIN_COLOR,
      backgroundColor: H3lp3rColors.BACKGROUND_RESULT_COLOR,
      padding: 5,
      borderRadius: 8,
      flexDirection: 'row',    
      elevation: 0,
    },
    result_text: {
      flex: 1,
      marginRight: 5,
      padding: 5,
      fontFamily: MONOSPACE_FONT_FAMLIY,
      fontSize: 18,
    }
  });
  