
import H3lp3rColors from "@app/constants/Colors";
import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { IconButton } from "./Themed";


export type TextAreaWithiconProps = {
  text?: string;
  numberOfLines?: number;
  onChangeText: (txt: string) => void;
}

export function TextAreaWithIcon(props: TextAreaWithiconProps) {
  const { onChangeText, text, numberOfLines } = props;

  return (
      <View style={styles.input_container}>
        <TextInput
          style={styles.input}
          autoCorrect={false}
          autoFocus={true}
          value={text}        
          returnKeyType={"done"}
          onChangeText={onChangeText} 
          multiline={true}
          textAlignVertical="top"
          numberOfLines={numberOfLines || 4}          
        />
        {!!text && <IconButton 
            icon="trash-bin-outline" size={14} 
            bgColor={H3lp3rColors.BACKGROUND_COLOR} 
            tintColor={H3lp3rColors.TEXT_COLOR} action={() => onChangeText('')} />}

      </View>
  );
}

const styles = StyleSheet.create({
  input_container: {
    width: '80%',
    borderWidth: 1,
    borderColor: H3lp3rColors.TEXT_COLOR,
    padding: 10,
    borderRadius: 8,
    elevation: 5,
    backgroundColor: H3lp3rColors.BACKGROUND_COLOR,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  input: {
    flex: 1,
    maxHeight: 125,
  },

});

