import { Platform } from "react-native";

export const URL_WEB_QO = 'https://qualityobjects.com';
export const URL_WEB_H3LP3R = 'https://h3lp3r.qodev.es';

export const MONOSPACE_FONT_FAMLIY = Platform.OS === 'ios' ? 'Menlo' : 'monospace';