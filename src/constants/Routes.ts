import Base64Screen from "@app/screens/Base64Screen"
import DashboardScreen from "@app/screens/DashboardScreen"
import HashScreen from "@app/screens/HashScreen"
import OracleScreen from "@app/screens/OracleScreen"
import RandomScreen from "@app/screens/RandomScreen"
import { Ionicons } from "@expo/vector-icons"


export const H3lp3rDrawerRoutesList = {
  DashboardRoute: {
  },
  HashActionRouteMD5: {
    algorithm: 'md5'
  },
  HashActionRouteSHA1: {
    algorithm: 'sha1'
  },
  HashActionRouteSHA256: {
    algorithm: 'sha256'
  },
  HashActionRouteSHA512: {
    algorithm: 'sha512'
  },
  Base64ActionRouteEncoder: {
    action: 'encode'
  },
  Base64ActionRouteDecoder: {
    action: 'decode'
  },
  AskToOracleAction: {
  },
  RandomIntegerAction: {
    type: 'integer'
  },
  RandomDecimalAction: {
    type: 'decimal'
  },
  RandomNamesAction: {
    type: 'names'
  },
};
 
export type H3lp3rRouteData = {
  routeName: keyof typeof H3lp3rDrawerRoutesList,
  group: 'home' |'hash' | 'base64' | 'oracle' | 'random',
  headerTitle: string,
  drawerLabel: string,
  drawerIcon: React.ComponentProps<typeof Ionicons>['name'],
  screen: React.ComponentType<any>,
  initialParams?: any
}

export const MY_ROUTES: H3lp3rRouteData[] = [
  {
    routeName: 'DashboardRoute',
    group: 'home',
    headerTitle: 'Dashboard',
    drawerLabel: 'Home',
    drawerIcon: 'home-outline',
    screen: DashboardScreen
  },
  {
    routeName: 'HashActionRouteMD5',
    group: 'hash',
    headerTitle: 'Hash MD5',
    drawerLabel: 'MD5',
    drawerIcon: 'code-working-outline',
    screen: HashScreen,
    initialParams: H3lp3rDrawerRoutesList['HashActionRouteMD5']
  },
  {
    routeName: 'HashActionRouteSHA1',
    group: 'hash',
    headerTitle: 'Hash SHA1',
    drawerLabel: 'SHA1',
    drawerIcon: 'code-working-outline',
    screen: HashScreen,
    initialParams: H3lp3rDrawerRoutesList['HashActionRouteSHA1']
  },
  {
    routeName: 'HashActionRouteSHA256',
    group: 'hash',
    headerTitle: 'Hash SHA256',
    drawerLabel: 'SHA256',
    drawerIcon: 'code-working-outline',
    screen: HashScreen,
    initialParams: H3lp3rDrawerRoutesList['HashActionRouteSHA256']
  },
  {
    routeName: 'HashActionRouteSHA512',
    group: 'hash',
    headerTitle: 'Hash SHA512',
    drawerLabel: 'SHA512',
    drawerIcon: 'code-working-outline',
    screen: HashScreen,
    initialParams: H3lp3rDrawerRoutesList['HashActionRouteSHA512']
  },
  {
    routeName: 'Base64ActionRouteEncoder',
    group: 'base64',
    headerTitle: 'Base64 - Encoder',
    drawerLabel: 'Base64 encoder',
    drawerIcon: 'swap-horizontal-outline',
    screen: Base64Screen,
    initialParams: H3lp3rDrawerRoutesList['Base64ActionRouteEncoder']
  },
  {
    routeName: 'Base64ActionRouteDecoder',
    group: 'base64',
    headerTitle: 'Base64 - Decoder',
    drawerLabel: 'Base64 decoder',
    drawerIcon: 'swap-horizontal-outline',
    screen: Base64Screen,
    initialParams: H3lp3rDrawerRoutesList['Base64ActionRouteDecoder']
  },
  {
    routeName: 'AskToOracleAction',
    group: 'oracle',
    headerTitle: 'Oracle',
    drawerLabel: 'Ask to the Oracle',
    drawerIcon: 'help-outline',
    screen: OracleScreen,
    initialParams: H3lp3rDrawerRoutesList['AskToOracleAction']
  },
  {
    routeName: 'RandomIntegerAction',
    group: 'random',
    headerTitle: 'Random integer',
    drawerLabel: 'Random integer',
    drawerIcon: 'flower-outline',
    screen: RandomScreen,
    initialParams: H3lp3rDrawerRoutesList['RandomIntegerAction']
  },
  {
    routeName: 'RandomDecimalAction',
    group: 'random',
    headerTitle: 'Random decimal',
    drawerLabel: 'Random decimal',
    drawerIcon: 'flower-outline',
    screen: RandomScreen,
    initialParams: H3lp3rDrawerRoutesList['RandomDecimalAction']
  },
  {
    routeName: 'RandomNamesAction',
    group: 'random',
    headerTitle: 'Names generator',
    drawerLabel: 'Names generator',
    drawerIcon: 'flower-outline',
    screen: RandomScreen,
    initialParams: H3lp3rDrawerRoutesList['RandomNamesAction']
  },
] // 
