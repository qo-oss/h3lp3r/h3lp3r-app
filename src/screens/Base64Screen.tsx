
import { Footer } from '@app/components/Footer';
import { B64ActionType, base64Service } from '@app/services';
import { RouteProp } from '@react-navigation/core';
import { DrawerActionHelpers } from '@react-navigation/routers';
import * as React from 'react';
import { View } from 'react-native';
import { StringForm } from '../components/StringForm';


export default class Base64Screen extends React.Component {
  action: B64ActionType;
  actionLabel: string;

  constructor (props: {navigation: DrawerActionHelpers<any>, route: RouteProp<{ params: { action: B64ActionType } }, 'params'>}) {
    super(props);
    this.action = props.route.params.action;
    this.actionLabel = this.action === 'encode' ? 'Encode' : 'Decode';
  }

  render() { 
    return ( 
      <View style={{ flex: 1, }}>
        <StringForm title={`Base64 ${this.action}`} actionLabel={this.actionLabel} actionCaller={(txt: string) => this.executeAction(txt)} />
        <Footer />
      </View>

    );
  }

  async executeAction(txt: string) : Promise<string> {    
    return base64Service.base64(this.action, txt);
  }

}



