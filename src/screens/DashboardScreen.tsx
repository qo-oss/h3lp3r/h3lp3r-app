import { FavsSection } from '@app/components/Favs';
import { Footer } from '@app/components/Footer';
import { favoritesStore } from '@app/stores';
import { NavigationProp, Route } from '@react-navigation/core';
import { getSnapshot, IDisposer, onSnapshot } from 'mobx-state-tree';
import * as React from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';


export default class DashboardScreen extends React.Component<{}, {favs: any[], execs: any[]}> {
  route: Route<any>;
  navigation: NavigationProp<any>;
  disposers: IDisposer[] = [];

  constructor (props: {route: Route<any>, navigation: NavigationProp<any>}) {
    super(props);
    this.route = props.route;
    this.navigation = props.navigation;
    
  }
  componentDidMount() {
    this._updateFavs();
    this.disposers.push(onSnapshot(favoritesStore, (newValue) => {
      this._updateFavs(newValue.actions);
    }));
  }
  componentWillUnmount() {
    this.disposers.forEach(disposer => disposer());
  }

  _updateFavs(newFavs?: any[]) {
    super.setState({
      favs: newFavs || getSnapshot(favoritesStore).actions
    });
    
  }

  render() { 
    const image = require("@app/assets/images/qo_logo_watermark.png");

    const favs = this.state?.favs || [];
    return (
      <View style={styles.container}>
        <View style={[styles.container, {padding: 20}]}>
          <ImageBackground source={image} style={styles.image} resizeMode="contain">
            <FavsSection favs={favs} navigation={this.navigation} />
          </ImageBackground>
        </View>
        <Footer />
      </View>
    );
  }

  goTo(routeName: string) {
    this.navigation.navigate(routeName);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
});



