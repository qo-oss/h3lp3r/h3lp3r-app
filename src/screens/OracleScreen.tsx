
import { Footer } from '@app/components/Footer';
import { StringResult } from '@app/components/StringResult';
import { TextAreaWithIcon } from '@app/components/TextAreaWithIcon';
import { ActionButton, IconButton } from '@app/components/Themed';
import { MONOSPACE_FONT_FAMLIY } from '@app/constants';
import H3lp3rColors from '@app/constants/Colors';
import { AlgorithmType, hashService, oracleService, QuestionType } from '@app/services';
import { DrawerActionHelpers, RouteProp } from '@react-navigation/core';
import * as React from 'react';
import { Keyboard, Platform, Pressable, StyleSheet, Text, TextInput, View } from 'react-native';
import RadioGroup, { RadioButtonProps } from 'react-native-radio-buttons-group';


export default class OracleScreen extends React.Component {
  navigation: DrawerActionHelpers<any>;
  algorithm: AlgorithmType;

  constructor(props: { navigation: DrawerActionHelpers<any>, route: RouteProp<{ params: { algorithm: AlgorithmType } }, 'params'> }) {
    super(props);
    this.navigation = props.navigation;
    this.algorithm = props.route.params.algorithm;
  }

  render() {
    return (
      <View style={{ flex: 1, }}>
        <OracleForm actionCaller={(type: QuestionType, question?: string) => this.executeAction(type, question)} />
        <Footer />
      </View>
    );
  }

  async executeAction(type: QuestionType, question?: string): Promise<string> {
    return oracleService.ask(type, question);
  }

  openSideMenu() {
    this.navigation.toggleDrawer();
  }
}

const ANSWER_TYPE_RADIO_BUTTONS = [
  {
    id: 'YES_NO',
    label: 'Yes / No',
    value: 'YES_NO',
    selected: true
  },
  {
    id: 'YES_NO_MAYBE',
    label: 'Yes / No / Maybe',
    value: 'YES_NO_MAYBE' as QuestionType
  },
]



export function OracleForm(props: {
  actionCaller: (type: QuestionType, question?: string) => Promise<string>
}) {
  const { actionCaller } = props;
  const [question, setQuestion] = React.useState('');
  const [result, setResult] = React.useState('');
  const [type, setType] = React.useState<QuestionType>('YES_NO');


  async function executeAction() {
    setResult(await actionCaller(type, question));
  }

  return (
    <Pressable onPress={Keyboard.dismiss} accessible={false} style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.input_label}>Ask your question to the Oracle</Text>
        <TextAreaWithIcon 
          text={question} 
          numberOfLines={2}
          onChangeText={(txt) => setQuestion(txt)} ></TextAreaWithIcon>
        <Text style={styles.input_label}>Answer type</Text>
        <View style={styles.input_container}>
          <RadioGroup
            onPress={(list) => {
              const { value } = list.find(o => o.selected) || { value: undefined};
              setType(value as QuestionType);
            }}
            radioButtons={ANSWER_TYPE_RADIO_BUTTONS}
          />
        </View>
        {!!result && <StringResult result={result} hideCopy={true} />}
        <ActionButton icon="help-outline" title={'Ask'}
          action={executeAction}
          tintColor={H3lp3rColors.MAIN_COLOR_REVERSE}
          style={{ marginTop: 20 }} />
      </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  input_label: {
    textAlign: 'left',
    fontSize: 12,
    color: H3lp3rColors.TEXT_HINT_COLOR,
    width: '80%',
  },
  input_container: {
    width: '80%',
    borderWidth: 1,
    borderColor: H3lp3rColors.TEXT_COLOR,
    padding: 10,
    borderRadius: 8,
    elevation: 5,
    backgroundColor: H3lp3rColors.BACKGROUND_COLOR,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  input: {
    flex: 1,
    maxHeight: 125,
  },
  result_container: {
    width: '80%',
    marginTop: 10,
    borderWidth: 1,
    borderColor: H3lp3rColors.MAIN_COLOR,
    backgroundColor: H3lp3rColors.BACKGROUND_RESULT_COLOR,
    padding: 5,
    borderRadius: 8,
    flexDirection: 'row',
    elevation: 0,
  },
  result_text: {
    flex: 1,
    marginRight: 5,
    padding: 5,
    fontFamily: MONOSPACE_FONT_FAMLIY,
    fontSize: 18,
  }
});



