
import { Footer } from '@app/components/Footer';
import { ActionButton, IconButton } from '@app/components/Themed';
import { MONOSPACE_FONT_FAMLIY } from '@app/constants';
import H3lp3rColors from '@app/constants/Colors';
import { RandomNameType, QuestionType, RandomActionType, RandomParamTypes, randomService } from '@app/services';
import { RouteProp } from '@react-navigation/core';
import { DrawerActionHelpers } from '@react-navigation/routers';
import * as React from 'react';
import { ListRenderItemInfo, Keyboard, Pressable, StyleSheet, Text, TextInput, View } from 'react-native';
import { StringResult } from '@app/components/StringResult';
import { FlatList } from 'react-native-gesture-handler';
import { Ionicons } from '@expo/vector-icons';
import Clipboard from 'expo-clipboard';


export default class RandomScreen extends React.Component {
  type: RandomActionType;

  constructor(props: { navigation: DrawerActionHelpers<any>, route: RouteProp<{ params: { type: RandomActionType } }, 'params'> }) {
    super(props);
    this.type = props.route.params.type;
  }

  render() {
    return (
      <View style={{ flex: 1, }}>
        <RandomForm type={this.type} actionCaller={(params: RandomParamTypes) => this.executeAction(params)} />
        <Footer />
      </View>

    );
  }

  async executeAction(params: RandomParamTypes): Promise<string | RandomNameType[]> {
    return randomService.random(this.type, params);
  }

}

export function RandomForm(props: {
  type: RandomActionType,
  actionCaller: (params: RandomParamTypes) => Promise<string | RandomNameType[]>
}) {
  const { actionCaller, type } = props;
  const [result, setResult] = React.useState<any>(null);

  const [initialParams, setInitialParams] = React.useState<RandomParamTypes>({ total: 10});
  const [params, setParams] = React.useState<RandomParamTypes>(initialParams);

  async function executeAction() {
    setInitialParams(params);
    const result = await actionCaller(params);
    if (type === 'names') {
      setResult(result);
    } else {
      setResult(`${result}`);
    }

  }

  return (
    <Pressable onPress={Keyboard.dismiss} accessible={false} style={{ flex: 1 }}>
      <View style={styles.container}>
        {type !== 'names' && <RandomNumberInput type={type} onParamsChange={(p: RandomParamTypes) => setParams(p)} initialParams={initialParams} onSubmit={executeAction} />}
        {type === 'names' && <TotalNamesInput onChange={(total: number) => setParams({ total })} initialTotal={10} onSubmit={executeAction} />}
        {!!result && type !== 'names' && <StringResult result={result} />}
        {!!result && type === 'names' && <ListNamesResult result={result} />}

        <ActionButton icon="play-circle-outline" title={'Generate'}
          action={executeAction}
          tintColor={H3lp3rColors.MAIN_COLOR_REVERSE}
          style={{ marginTop: 20 }} />
      </View>
    </Pressable>
  );
}

function RandomNumberInput(props: any) {
  const { onSubmit, onParamsChange, type, initialParams } = props;
  const [params, setParams] = React.useState<RandomParamTypes>(initialParams || {});
  const [editedParams, setEditedParams] = React.useState<any>(initialParams || {});


  async function onChangeParamNumber(minmax: 'min' | 'max', value: string) {
    const strNumber = (type === 'int') ? value.replace(/[\,\.]/g, '') : value.replace(/\,/g, '.');

    const val = +strNumber;
    params[minmax] = val;
    onParamsChange(params);
    setParams(params);

    editedParams[minmax] = value;
    setEditedParams({ ...editedParams });
  }

  return (
    <View style={styles.container_numbers}>
      <View style={{ flexBasis: '45%' }}>
        <Text style={styles.input_label}>Min value</Text>
        <View style={styles.input_container}>
          <TextInput
            keyboardType={type === 'decimal' ? 'decimal-pad' : 'numeric'}
            style={styles.input}
            autoCorrect={false}
            autoFocus={false}
            value={`${editedParams.min || ''}`}
            returnKeyType={"done"}
            onChangeText={(txt) => onChangeParamNumber('min', txt)}
            onSubmitEditing={onSubmit}
            multiline={false}
          />
        </View>
      </View>
      <View style={{ flexBasis: '45%' }}>
        <Text style={styles.input_label}>Max value</Text>
        <View style={styles.input_container}>
          <TextInput
            keyboardType={type === 'decimal' ? 'decimal-pad' : 'number-pad'}
            style={styles.input}
            autoCorrect={false}
            autoFocus={false}
            returnKeyType={"done"}
            value={`${editedParams.max || ''}`}
            onChangeText={(txt) => onChangeParamNumber('max', txt)}
            onSubmitEditing={onSubmit}
            multiline={false}
          />
        </View>
      </View>
    </View>
  );
}

function TotalNamesInput(props: any) {
  const { onSubmit, onChange, initialTotal } = props;
  const [total, setTotal] = React.useState<number>(+initialTotal || 1);


  async function onValueChange(value: string) {
    const strNumber = value.replace(/[\,\.]/g, '');

    const val = (strNumber && +strNumber);
    onChange(`${val}`);
    setTotal(val as number);
  }

  return (
    <View style={{ width: '50%' }}>
      <Text style={styles.input_label}>Total names to generate</Text>
      <View style={styles.input_container}>
        <TextInput
          keyboardType={'number-pad'}
          style={{ flex: 1 }}
          autoCorrect={false}
          autoFocus={false}
          value={`${total || ''}`}
          returnKeyType={"done"}
          onChangeText={(txt) => onValueChange(txt)}
          onSubmitEditing={onSubmit}
          multiline={false}
        />
      </View>
    </View>
  );
}

const LIMIT_NAMES = 100;

function ListNamesResult(props: { result: RandomNameType[] }) {
  const { result } = props;
  const [copying, setCopying] = React.useState(false);
  const data = (result && result.slice(0, LIMIT_NAMES).map((item, index) => ({ ...item, key: '' + index }))) || [];

  function renderItem(ele: ListRenderItemInfo<RandomNameType>) {
    const item = ele.item;
    const icon = item.gender === 'MALE' ? 'man-outline' : 'woman-outline';
    const iconColor = item.gender === 'MALE' ? 'orange' : 'green';
    return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <Ionicons name={icon} size={24} color={iconColor} />
      <Text style={{ color: copying ? H3lp3rColors.MAIN_COLOR : H3lp3rColors.TEXT_COLOR }}>{item.firstName} {item.lastName}</Text>
    </View>);
  }

  function copyText() {
    Clipboard.setString(JSON.stringify(result, null, 2));
  }

  return (
    <View style={{ ...styles.result_container, marginTop: 20, maxHeight: '66%' }}>
      <View style={{ flex: 1 }}>
        <FlatList style={{ flex: 0, flexGrow: 1 }}
          data={data}
          renderItem={(item) => renderItem(item)}
        />
        {(result?.length > LIMIT_NAMES) && <Text style={{ ...styles.result_text, flex: 0, fontSize: 12, opacity: 0.7, flexWrap: 'wrap' }}>
          Only the first 100 results are shown. Touch in "copy" button to get the entire {result.length} items
          </Text>}
      </View>
      <IconButton onPressIn={() => setCopying(true)}
        onPressOut={() => setCopying(false)}
        icon="copy-outline" size={24}
        bgColor={H3lp3rColors.BACKGROUND_COLOR}
        tintColor={H3lp3rColors.TEXT_RESULT_COLOR} action={copyText} />
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  container_numbers: {
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-between'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  input_label: {
    textAlign: 'left',
    fontSize: 12,
    color: H3lp3rColors.TEXT_HINT_COLOR,
    width: '80%',
  },
  input_container: {
    borderWidth: 1,
    borderColor: H3lp3rColors.TEXT_COLOR,
    padding: 10,
    borderRadius: 8,
    elevation: 5,
    backgroundColor: H3lp3rColors.BACKGROUND_COLOR,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  input: {
    flex: 1,
    maxHeight: 125,
  },
  result_container: {
    width: '80%',
    marginTop: 10,
    borderWidth: 1,
    borderColor: H3lp3rColors.MAIN_COLOR,
    backgroundColor: H3lp3rColors.BACKGROUND_RESULT_COLOR,
    padding: 5,
    borderRadius: 8,
    flexDirection: 'row',
    elevation: 0,
  },
  result_text: {
    flex: 1,
    marginRight: 5,
    padding: 5,
    fontFamily: MONOSPACE_FONT_FAMLIY,
    fontSize: 18,
  }
});


/*

useEffect(() => {
  Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
  Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

  // cleanup function
  return () => {
    Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
  };
}, []);
*/