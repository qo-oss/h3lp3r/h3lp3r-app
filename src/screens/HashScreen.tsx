
import { Footer } from '@app/components/Footer';
import { AlgorithmType, hashService } from '@app/services';
import { DrawerActionHelpers, RouteProp } from '@react-navigation/core';
import * as React from 'react';
import { View } from 'react-native';
import { StringForm } from '../components/StringForm';


export default class HashScreen extends React.Component {
  navigation: DrawerActionHelpers<any>;
  algorithm: AlgorithmType;

  constructor (props: {navigation: DrawerActionHelpers<any>, route: RouteProp<{ params: { algorithm: AlgorithmType } }, 'params'>}) {
    super(props);
    this.navigation = props.navigation;
    this.algorithm = props.route.params.algorithm;
  }

  render() { 
    return (
      <View style={{ flex: 1, }}>
        <StringForm title={this.algorithm} actionLabel="Execute" actionCaller={(txt: string) => this.executeAction(txt)} />
        <Footer />
      </View>
    );
  }

  async executeAction(txt: string) : Promise<string> {    
    return hashService.getHash(this.algorithm, txt);
  }

  openSideMenu() {
    this.navigation.toggleDrawer();
  }
}



