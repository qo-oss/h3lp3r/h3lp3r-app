import { URL_BASE } from "@app/constants/Server";
import { getQueryParams } from "@app/utils";

export type AlgorithmType = 'md5' | 'sha1' | 'sha256' | 'sha512';
export type B64ActionType = 'encode' | 'decode';
export type RandomActionType = 'int' | 'decimal' | 'names';
export type QuestionType = 'YES_NO' | 'YES_NO_MAYBE';

export type RandomParamTypes = {
    min?: number;
    max?: number;
    total?: number;
};

export type OracleParamTypes = {
    question?: string;
    type?: QuestionType;
};

export type GenderType = 'MALE' | 'FEMALE';

export type RandomNameType = {
    gender: GenderType;
    firstName: string;
    lastName: string;
}

abstract class ServiceBase {
    async call(url: string, options?: RequestInit) : Promise<any> {
        const response = await fetch(url, options);
        if (response.status >= 200 || response.status < 300) {
            return response.json();
        }
        throw new Error(`Error accessing to server: ${response.status}`);
    }
}

class HashService extends ServiceBase {

    async getHash(algorithm: AlgorithmType, txt: string) : Promise<string> {
        if (!txt) {
            return '';
        }
        const url = `${URL_BASE}/hash/${algorithm}?text=${encodeURIComponent(txt)}`;
        const { result } = await super.call(url);
        return result;
    }
}

class Base64Service extends ServiceBase {

    async base64(action: B64ActionType, txt: string) : Promise<string> {
        if (!txt) {
            return '';
        }
        const url = `${URL_BASE}/base/64/${action}?text=${encodeURIComponent(txt)}`;
        const { result } = await super.call(url);
        return result;
    }
    async encode(txt: string) : Promise<string> {        
        return this.base64('encode', txt);
    }
    async decode(txt: string) : Promise<string> {        
        return this.base64('decode', txt);
    }
}

class OracleService extends ServiceBase {

    async ask(questionType: QuestionType, question?: string) : Promise<string> {
        const url = `${URL_BASE}/random/oracle?${getQueryParams({type: questionType, question})}`;
        const { result } = await super.call(url);
        return result;
    }
}

class RandomService extends ServiceBase {

    async random(type: RandomActionType, params: RandomParamTypes) : Promise<string | RandomNameType[]> {
        const type_url = type === 'names' ? 'names' : `number/${type}` ;
        const url = `${URL_BASE}/random/${type_url}?${getQueryParams(params)}`;
        const { result } = await super.call(url);
        return result;
    }
}

class IpService extends ServiceBase {

    async getIP() : Promise<string> {
        const url = `${URL_BASE}/ip`;
        const response = await fetch(url);
        if (response.status >= 200 || response.status < 300) {
            return response.text();
        }
        return '';
    }
}

export const hashService = new HashService();
export const base64Service = new Base64Service();
export const oracleService = new OracleService();
export const randomService = new RandomService();
export const ipService = new IpService();
