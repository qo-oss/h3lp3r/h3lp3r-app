import AsyncStorage from '@react-native-async-storage/async-storage';
import { applySnapshot, IAnyStateTreeNode, IAnyType, onSnapshot } from 'mobx-state-tree';



export const  makePersistent = (name: string, store: IAnyStateTreeNode) => {
  
  const storeName = `@store_${name}`;
  
  onSnapshot(store, (snapshot: any) => {
    const data = JSON.stringify(snapshot);
    AsyncStorage.setItem(storeName, data)
  });

  AsyncStorage.getItem(storeName)
  .then((initialData: string | null) => {
      if (!!initialData) {
        const initSnapshot = JSON.parse(initialData);
        applySnapshot(store, initSnapshot);
      }
    });

}