import AsyncStorage from '@react-native-async-storage/async-storage';
import { IModelType, types } from "mobx-state-tree"
import { makePersistent } from "./persist";

const Action = types.model({
    name: types.identifier,
    icon: types.string,
    title: types.string
});

const Favorites = types.model({
    actions: types.array(Action)
}).views(self => ({
    contains(name: string): boolean {
        return !!self.actions.find(a => a.name == name);
    },
    get size(): number {
        return self.actions.length;
    }
})).actions((self) => ({
    add(name: string, icon: string, title: string): void {
        if (!self.contains(name)) {
            self.actions.push({ name, icon, title });
        }
    },
    remove(name: string): void {
        const action2remove = self.actions.find(a => a.name === name);
        !!action2remove && self.actions.remove(action2remove);
    }
}));


export const favoritesStore = Favorites.create();

makePersistent('FAVS', favoritesStore);