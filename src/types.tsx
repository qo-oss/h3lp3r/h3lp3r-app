import { Ionicons } from "@expo/vector-icons";
import { AlgorithmType, B64ActionType } from "./services";

export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type H3lp3rGroups = 'hash' | 'base64' | 'oracle' | 'random';

export type H3lp3rIcon = React.ComponentProps<typeof Ionicons>['name'];