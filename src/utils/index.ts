
export const capitalize = (txt: string) =>  txt && (txt[0].toUpperCase() + txt.slice(1));

export const getQueryParams = (params: any) : string => {
    if (!params) {
        return '';
    }
    return Object.keys(params).filter(k => !!params[k]).map(k => `${k}=${encodeURIComponent(params[k])}`).join('&');    
} 
