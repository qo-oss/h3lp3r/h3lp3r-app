import * as Linking from 'expo-linking';
import { H3lp3rDrawerRoutesList } from '../constants/Routes';


let screens : {[key: string]: any} = {};
Object.keys(H3lp3rDrawerRoutesList).forEach((k) => screens[k] = { path: k});

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {    
    initialRouteName: typeof H3lp3rDrawerRoutesList.DashboardRoute,    
    screens: {
      Root: { screens },
      NotFound: '*',
      },      
  },  
};

//