import { CustomDrawerContent } from '@app/components/CustomDrawerContent';
import H3lp3rColors from '@app/constants/Colors';
import { MY_ROUTES } from '@app/constants/Routes';
import { favoritesStore } from '@app/stores';
import { Ionicons  } from '@expo/vector-icons';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { DrawerActionHelpers, Route, Router } from '@react-navigation/routers';
import { getSnapshot } from 'mobx-state-tree';
import React, { useState } from 'react';
import { Keyboard, Pressable } from 'react-native';
import { H3lp3rDrawerRoutesList } from '../constants/Routes';


const Drawer = createDrawerNavigator<typeof H3lp3rDrawerRoutesList>();

export default function SideMenuNavigator() {

  return (
    <Drawer.Navigator          
      drawerContent={(props: any) => <CustomDrawerContent {...props} /> }
      hideStatusBar={false}      
      screenOptions={(props) => ({
        headerStatusBarHeight: 20,
        headerTintColor: H3lp3rColors.MAIN_COLOR_REVERSE,
        headerStyle: {
          backgroundColor: H3lp3rColors.MAIN_COLOR
        },
        headerShown: true,
        headerLeft: ({tintColor}) => LeftTitleButton(props.navigation, tintColor),
        headerRight:  () => FavButton(props.route),
      })}
      >
        {MY_ROUTES.map(r => (<Drawer.Screen
        key={r.routeName}
        name={r.routeName}
        component={r.screen}
        initialParams={{...r.initialParams}}
        options={{          
          drawerLabel: r.drawerLabel,
          headerTitle: r.headerTitle,
          drawerIcon: ({ color }) => <DrawerIcon name={r.drawerIcon} color={color} />,
        }}
      />))}
      
    </Drawer.Navigator>
  );
}


function LeftTitleButton(navigation: DrawerActionHelpers<any>, tintColor?: string) {
  return (
    <Pressable onPress={() => {
      Keyboard.dismiss();
      navigation.toggleDrawer();
    }}     
      style={{ padding: 10, }}>
      <Ionicons name="menu" size={32} color={tintColor} /> 
    </Pressable>
  );
}


function FavButton(route: Route<any>) {
  type FavIcon = 'star' | 'star-outline';
  if (route.name === 'DashboardRoute') {
    return undefined;
  }
  const [isFav, setFav] = useState(favoritesStore.contains(route.name));
  const iconName : FavIcon = (isFav ? 'star' : 'star-outline');

  function toggleFav() {
    if (isFav) {
      favoritesStore.remove(route.name);
    } else {
      const routeData = MY_ROUTES.find(r => r.routeName === route.name);
      !!routeData && favoritesStore.add(route.name, routeData.drawerIcon, routeData.headerTitle);
    }
    setFav(!isFav);
  }
  return (
    <Pressable onPress={toggleFav}
      style={{ padding: 10, }}>
      <Ionicons name={iconName} size={28} color={H3lp3rColors.MAIN_COLOR_REVERSE} /> 
    </Pressable>
  );
}


// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function DrawerIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={28} style={{ marginBottom: 0 }} {...props} />;
}
