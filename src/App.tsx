import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import useCachedResources from './hooks/useCachedResources';
import Navigation from './navigation';
import H3lp3rColors from './constants/Colors';

function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {
    return ( 
      <SafeAreaProvider>
        <Navigation />
        <StatusBar style="light" backgroundColor={H3lp3rColors.MAIN_COLOR} animated={true} networkActivityIndicatorVisible={true} />
      </SafeAreaProvider>
    );
  }
}

export default App;