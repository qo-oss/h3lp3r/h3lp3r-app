import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';




export default class App extends Component {
  render() {
    return (<View style={styles.container}>
        <Text style={{
          fontSize: 40,
          padding: 20,
          textAlign: 'center'
        }} >Welcome to QO Metup!</Text>
        <Pressable onPress={this.onPressFunction}>
          <Text>I'm pressable!</Text>
        </Pressable>
        <StatusBar style="auto" />
      </View>
    );
  }

  onPressFunction() {
    console.log('Ok');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0a0b0',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
